module.exports = {
    apps: [{
        name: "server",
        script: "./servers/server.js",
        instances: 1,
        log_date_format : "DD-MM HH:mm:ss.SSS"
    }]
};

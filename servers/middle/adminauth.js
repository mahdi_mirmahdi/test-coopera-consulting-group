const jwt = require('jsonwebtoken');
const {Member} = require('../models/Member');

exports.authentication = (req, res, next) => {

    try {
        let BearerToken = req.headers.authorization;
        let token = BearerToken.replace('Bearer ','');
        let decodedToken = jwt.verify(token, process.env.SECRETKEY);
        let _id = decodedToken._id;
        if (req.body._id && req.body._id !== _id) {
            throw 'Invalid user ID';
        } else {
            next();
        }
    } catch(e) {
        res.status(401).send(e);
    }
};

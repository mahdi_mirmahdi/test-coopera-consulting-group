const mongoose = require('mongoose');
let Schema = mongoose.Schema;
let StepSchema = new mongoose.Schema({

    course: {
        type: Schema.Types.ObjectId,
        ref: 'Course',
        required: true

    } ,
    stepNumber: {
        type: Number,
        required: true
    } ,
    questions : [
        {
            type: Schema.Types.ObjectId,
            ref: 'Question'
        }
    ]
});


StepSchema.index({ course: 1, stepNumber: 1 }, { unique: true });







let Step = mongoose.model('Step', StepSchema);

module.exports ={Step};

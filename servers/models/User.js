const mongoose = require('mongoose');
const validator = require('validator');

let UserSchema = new mongoose.Schema({

    name:{
        type: String,
        required: true
    },
    family:{
        type: String,
        required: true

    },
    position:{
        type: String,
        required: true
    },
    mail:{
            type: String,
            required: true,
            trim: true,
            validate:{
                validator: validator.isEmail ,
                message: `{value} is not a valid email`
            }
    },
    phoneNumber:{
        type: String,
        required: true
    },
    salary:{
        type: String,
        required: true
    },
    projectNumber:{
        type: Number,
        required: true,
        default: 0
    },
});










let User = mongoose.model('User', UserSchema);

module.exports ={User};

const persianMongoose =require('persian-mongoose');
const mongoose = require('mongoose');
const validator = require('validator');
const _ = require('lodash');
const jwt =require('jsonwebtoken');
const bcrypt = require('bcryptjs');
let MemberSchema = new mongoose.Schema({

    position:{
        type: String,
        required: true
    },
    mail:{
        unique: true,
        type: String,
        required: true,
        trim: true,
        validate:{
            validator: validator.isEmail ,
            message: `{value} is not a valid email`
        }
    },
    phoneNumber:{
        unique: true,
        type: String,
        required: true
    },
    name:{
        type: String,
        required: true
    },
    family:{
        type: String,
        required: true

    },
    address:{
        type: String,
        required: true

    },
    username:{
        unique: true,
        type: String,
        required: true,
        trim: true,

    },

    password:{
        type: String,
        required: true,
        trim: true,
        minLenght: 6
    }

});





MemberSchema.methods.generateAuthToken = function(){
    let member =this;
    let access = 'auth';

    let token = jwt.sign(
        {_id: member._id.toHexString(), access }
        , process.env.SECRETKEY, { expiresIn: '10m' }).toString();
    return member.save().then(()=>{
        return token ;
    })
};

MemberSchema.statics.FindByToken = function(token){
    let Member =this;
    let decoded;
    try {
        decoded = jwt.verify(token , process.env.SECRETKEY);
        return Member.findOne({
            '_id' : decoded._id
        })
    }catch (e) {
        return null
    }

};


MemberSchema.statics.FindByCredentials = function(username , password){
    let Member =this;

    return Member.findOne({username}).then((member)=>{
        if(!member){
            return Promise.reject();
        }
        return new Promise((resolve , reject)=>{
            bcrypt.compare(password , member.password , (err , res)=>{
                if(res){
                    resolve(member)

                }else {
                    reject();

                }
            })
        })
    })
};

MemberSchema.pre('save', function(next){
    var member = this;
    if(member.isModified('password')){

        bcrypt.genSalt(5 , (err,salt)=>{
            bcrypt.hash(member.password, salt, (err,hash)=>{
                member.password =hash;
                next();
            })
        })
    }else{
        next();
    }
});

let Member = mongoose.model('Member', MemberSchema);

module.exports ={Member};

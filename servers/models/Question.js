const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let QuestionSchema = new mongoose.Schema({

    title: {
        type: String ,
        required: true

    } ,
    answer: {
        type: String,
        required: true
    },
    step: {
        type: Schema.Types.ObjectId,
        ref: 'Step',
        required: true

    } ,
});



let Question = mongoose.model('Question', QuestionSchema);

module.exports ={Question};

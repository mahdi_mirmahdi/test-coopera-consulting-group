const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let CourseSchema = new mongoose.Schema({

    name: {
        type: String,
        required: true,
        unique: true
    },
    step : [
        {
            type: Schema.Types.ObjectId,
            ref: 'Step'
        }
    ]
});










let Course = mongoose.model('Course', CourseSchema);

module.exports ={Course};

const mongoose = require('mongoose')

mongoose.Promise = global.Promise
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.connect(process.env.MONGODB , {useNewUrlParser: true , useUnifiedTopology: true, useFindAndModify: false} , (err , connect) => {
    if (err) {
        console.log(err)
    }
    console.log('mongo run')
    }
)


module.exports ={mongoose}

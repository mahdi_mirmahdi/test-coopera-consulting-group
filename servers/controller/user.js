const _ = require('lodash');
let {User} = require('../models/User');
let {ObjectID} = require('mongodb')
const {xlsx} = require('xlsx')
exports.create = async function (req,res) {
    var body = _.pick(req.body, ['name','family','mail','phoneNumber','position','salary','projectNumber']);
         try {
             let correctPhone = await phoneNumberChecker(body.phoneNumber);
             if (correctPhone) {
                 var user = new User({name: body.name, family: body.family , mail: body.mail , phoneNumber: body.phoneNumber , position: body.position , salary: body.salary , projectNumber: body.projectNumber });
                 user.save().then((response)=>{
                     if (response) {
                         res.status(200).send(response);
                     }
                 }).catch((err)=>{
                     res.status(409).send(err)
                 })
             } else {
                 res.status(422).send('phone Number is invalid')

             }
         }catch (e) {
             res.status(400).send(e)
         }


};
exports.delete = function (req,res) {
    let id = req.params.id
    if(!ObjectID.isValid(id)){
        return res.status(404).send();
    }
        try {
            User.findOneAndDelete({_id : id}).then((response) => {
                if (response) {
                    res.status(200).send(true)
                } else {
                    res.status(404).send('user not found')
                }
            })
        }catch (e) {
            res.status(500).send(e)
        }
};
exports.getUser = function (req,res) {
    try {
        User.find().then((response) => {
            if (response) {
                if (response) {
                    res.status(200).send(response)
                } else {
                    res.status(404).send('user not found');
                }
            }
            else {
                res.status(400).send()
            }
        })

    } catch (e) {
        res.status(500).send(e)
    }
};
function phoneNumberChecker(phoneNumber) {
    let phonePattern = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/;
    return !!phoneNumber.match(phonePattern);
}
function mailChecker(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
exports.update = function (req,res) {
    let id = req.params.id
    if(!ObjectID.isValid(id)){
        return res.status(404).send();
    }
  try {
      var body = _.pick(req.body, ['name','family','mail','phoneNumber','position','salary','projectNumber']);
      User.findOneAndUpdate({_id: id} , body , {new: true}).then((update) => {
          if (update) {
              res.status(200).send(update)
          }
          else {
              res.status(404).send('user not found')

          }
      })
  } catch (e) {
      res.status(500).send()
  }
}

exports.importData = async function (req,res) {
    var body = _.pick(req.body, ['excel']);
    let data = [];
    try {
      if (body.excel.length !== 0) {
          for (let item of body.excel) {
              let correctPhone = await phoneNumberChecker(item.phoneNumber.toString() || 0);
              let correctMail = await mailChecker(item.mail || 0);
                if (correctPhone && correctMail) {
                    let user = {
                        name: item.name || null,
                        family: item.family || null,
                        position : item.position || null,
                        mail: item.mail || null,
                        phoneNumber: item.phoneNumber || null,
                        salary: item.salary || null,
                        projectNumber: item.projectNumber || null,
                    }
                    data.push(user)
                }
          }
          User.insertMany(data).then((response) => {
              if (response) {
                  res.status(200).send({itemNumber: response.length.toString() , data: response})
              } else {
                  res.status(400).send()
              }
          }).catch((e) => {
              res.status(400).send(e)
          })

      } else {
          res.status(400).send('data null')

      }
    }catch(e) {
        res.status(500).send(e)

    }

}

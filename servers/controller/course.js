const _ = require('lodash');
let {Course} = require('../models/Course');
let {Step} = require('../models/Step');
let {Question} = require('../models/Question');
let {ObjectID} = require('mongodb')

exports.createCourseStep = async function (req,res) {
    var body = _.pick(req.body, [ 'course']);
         try {
             Course.findOne({_id: body.course}).then((newResponse) => {
               if (newResponse) {
                   Step.find({course: body.course}).limit(1).sort({ _id: -1 }).then((response) => {
                       let i = 0
                       if (response.length === 1) {

                           i = response[0].stepNumber +1
                       }
                       else {
                           i = 1;
                       }
                       if (response) {
                           let step = new Step({course: body.course , stepNumber : i});
                           step.save().then((stepResponse)=>{
                               if (stepResponse) {
                                   newResponse.step.push(stepResponse)
                                   Course.findOneAndUpdate({_id: newResponse._id} , {step: newResponse.step}).then((success) => {
                                       if (success){
                                           res.status(200).send(stepResponse);
                                       }else {
                                           res.status(400).send()
                                       }
                                   }).catch((e) => {
                                       res.status(400).send(e)

                                   })
                               } else{
                                   res.status(400).send()
                               }
                           }).catch((err)=>{
                               res.status(409).send(err)
                           });
                       } else {
                           res.status(404).send()

                       }

                   })
               }
               else {
                   res.status(404).send('Course not found')
               }
             }).catch((e) => {
                 res.status(404).send(e);
             })
         } catch (e) {
             res.status(500).send(e)
         }

};
exports.stepDeleteById = async function (req,res) {
    try {
        let id = req.params.id
        if(!ObjectID.isValid(id)){
            return res.status(404).send();
        }
        Step.findByIdAndDelete(id).then((response)=>{
            if(response){
                Question.deleteMany({step : id }, {new: true}).then((newResponse) => {
                    if (newResponse) {
                        Course.findOneAndUpdate({_id : response.course },   { $pull: { step: id } } , {new: true}).then((newRes) => {
                            if (newRes) {
                                res.status(200).send(newRes)
                            } else {
                                return res.status(400).send();

                            }
                        }).catch((e) => {

                            return res.status(400).send(e);

                        })
                    } else {
                        return res.status(400).send();

                    }
                }).catch((e) => {

                    return res.status(400).send(e);

                })
            } else {
                return res.status(404).send();
            }

        }).catch((err)=>{
            res.status(400).send();
        })
    } catch (e) { res.status(500).send(e)}

};

exports.courseUpdateById = async function (req,res) {
    try {
        let id = req.params.id
        let body =_.pick(req.body, ['name']);
        if(!ObjectID.isValid(id)){
            return res.status(404).send();
        }
        Course.findByIdAndUpdate(id, {$set: body},{new: true}).then((response)=>{
            if(!response){
                return res.status(404).send();
            }
            res.status(200).send({response})
        }).catch((err)=>{
            res.status(400).send(err);
        })
    } catch (e) { res.status(500).send(e)}

};
exports.createCourse = async function (req,res) {
    var body = _.pick(req.body, ['name']);
    try {
        var course = new Course({name: body.name});
        course.save().then((response)=>{
            if (response) {
                res.status(200).send(response);
            } else {
                res.status(400).send()
            }
        }).catch((err)=>{
            res.status(409).send(err)
        })
    }catch (e) {
        res.status(500).send(e)
    }


};
exports.getCourse = async function (req,res) {
    try {
        Course.find().populate('step').exec(function (err, docs) {
            Step.populate(docs, {
                path: 'step.questions',
                model: Question
            }).then((x) => {
                res.send(x)
            })

        })

    } catch (e) { res.status(500).send(e)}

};
exports.deleteCourseById = async function (req , res) {
    try {

        let id = req.params.id
        if(!ObjectID.isValid(id)){
            return res.status(404).send();
        }
        Course.findByIdAndDelete(id).then((response)=>{
            if(response){
                Step.deleteMany({course : response._id } , {new: true}).then((newResponse) => {
                    if (newResponse) {

                        return res.status(200).send(newResponse);

                    } else {
                        return res.status(400).send();

                    }
                }).catch((e) => {

                    return res.status(400).send(e);

                })
            } else {
                return res.status(404).send();
            }

        }).catch((err)=>{
            res.status(400).send();
        })
    } catch (e) { res.status(500).send(e)}

}


exports.createStepQuestion = async function (req,res) {
    var body = _.pick(req.body, [ 'title' , 'answer' , 'step']);
    try {
        Step.findOne({_id: body.step}).then((stepResponse) => {
            if (stepResponse) {
                let question = new Question({title: body.title , answer: body.answer , step: stepResponse._id})
                question.save().then((questionResponse) => {
                    if (questionResponse) {
                        stepResponse.questions.push(questionResponse)
                        Step.findOneAndUpdate({_id: stepResponse._id} , {questions : stepResponse.questions}).then((response) => {
                            if (response) {
                                res.status(200).send(questionResponse)
                            } else {
                                res.status(400).send()
                            }
                        }).catch((e) => {
                            res.status(400).send(e)
                        })
                    } else {
                        res.status(400).send()
                    }
                }).catch((e) => {
                    res.status(400).send(e)
                })
            } else {
                res.status(404).send('Step not found')
            }
        }).catch((e) => {
            res.status(400).send(e)
        })
    } catch (e) {
        res.status(500).send(e)
    }


};
exports.questionDeleteById = async function (req,res) {
    try {
        let id = req.params.id
        if(!ObjectID.isValid(id)){
            return res.status(404).send();
        }
        Question.findByIdAndDelete(id).then((response)=>{
            if(response){
                Step.findOneAndUpdate({_id : response.step },   { $pull: { questions: id } } , {new: true}).then((newResponse) => {
                    if (newResponse) {
                        res.status(200).send(newResponse)
                    } else {
                        return res.status(400).send();

                    }
                }).catch((e) => {

                    return res.status(400).send(e);

                })
            } else {
                return res.status(404).send();
            }

        }).catch((err)=>{
            res.status(400).send();
        })} catch (e) { res.status(500).send(e)}

};
exports.questionUpdateById = async function (req,res) {
    try {
        let id = req.params.id
        let body =_.pick(req.body, ['title' , 'answer'])
        if(!ObjectID.isValid(id)){
            return res.status(404).send();
        }
        Question.findByIdAndUpdate(id, {$set: body},{new: true}).then((response)=>{
            if(!response){
                return res.status(404).send();
            }
            res.status(200).send({response})
        }).catch((err)=>{
            res.status(400).send(err);
        })
    } catch (e) { res.status(500).send(e)}


};


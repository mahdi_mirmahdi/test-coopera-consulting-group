const _ = require('lodash');
var {Member} = require('../models/Member');
var {Note} = require('../models/Note');
let {ObjectID} = require('mongodb')

exports.create = function (req,res) {
    try {
        var body = _.pick(req.body, ['color','text','username']);
        Member.findOne({username: body.username}).then((user) => {
            if (user) {
                var note = new Note({text: body.text, color: body.color , username: user._id });
                note.save().then((response)=>{
                    if (response) {
                        res.status(200).send(response);
                    }
                }).catch((err)=>{
                    res.status(400).send(err)
                })
            }
            else {
                res.status(401).send()
            }
        })
    } catch (e) {
        res.status(500).send()
    }

};
exports.delete = function (req,res) {
    try {
        let id = req.params.id
        let username = req.params.username
        if(!ObjectID.isValid(id)){
            return res.status(404).send();
        }
        Member.findOne({username: username}).then((user) => {
            if (user) {
                Note.findOneAndDelete({_id : id , username: user._id }).then((response) => {
                    if (response) {
                        res.status(200).send(true)
                    } else {
                        res.status(400).send()
                    }
                })
            }
            else {
                res.status(401).send()
            }
        })
    } catch (e) {
        res.status(500).send()
    }

};
exports.update = function (req,res) {
    try {

        let id = req.params.id
        let username = req.params.username
        if(!ObjectID.isValid(id)){
            return res.status(404).send();
        }
        var body = _.pick(req.body, [ 'text' , 'color' , 'type']);
        Member.findOne({username: username}).then((user) => {
            if (user) {
                if (body.type === 'color') {
                    Note.findOneAndUpdate({_id : id , username: user._id } , {color : body.color} , {new : true}).then((response) => {
                        if (response) {
                            res.status(200).send(true)
                        } else {
                            res.status(404).send()
                        }
                    }).catch((e) => {
                        res.status(400).send(e)
                    })

                }
                else if (body.type === 'text'){
                    Note.findOneAndUpdate({_id : id , username: user._id } , {text: body.text} , {new : true}).then((response) => {
                        if (response) {
                            res.status(200).send(true)
                        } else {
                            res.status(400).send()
                        }
                    })
                } else {
                    res.status(400).send(false)
                }
            }
            else {
                res.status(401).send()
            }
        })
    } catch (e) {
        res.status(500).send()
    }

};
exports.getNote = function (req,res) {
    try {
        let username = req.params.username
        Member.findOne({username: username}).then((user) => {
            if (user) {
                Note.find({username: user._id }).then((response) => {
                    if (response) {
                        res.status(200).send(response)
                    } else {
                        res.status(400).send()
                    }
                })
            }
            else {
                res.status(401).send()
            }
        })
    } catch (e) {
        res.status(500).send()
    }

};

var {Member} = require('../models/Member');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
let {ObjectID} = require('mongodb')

exports.login = function (req,res) {
    try { var body = _.pick(req.body, ['username' ,'password']);
        console.log('login  ' , body.username , body.password)

        Member.FindByCredentials(body.username , body.password).then((response)=>{
            response.generateAuthToken().then((token)=>{
                var user = {
                    username: body.username,
                    token : token
                }
                res.header('authorization', token)
                res.header('username', body.username)
                res.status(200).send(user);
            }) . catch((e) => {
                console.log(e)
                res.status(401).send('Invalid Username or Password');
            })
        }).catch((e)=>{
            console.log(e)

            res.status(401).send('Invalid Username or Password');

        })
    }  catch (e) {
        console.log(e)
        res.status(500).send(e)
    }


};
exports.getMember = function (req,res) {


    let username = req.params.username
    try {
        let BearerToken = req.headers.authorization;
        let token = BearerToken.replace('Bearer ','');
        let decodedToken = jwt.verify(token, process.env.SECRETKEY);
        let _id = decodedToken._id;
        Member.findOne({username: username , _id: _id}).then((response)=>{
            if (response) {
                let body = {
                    position: response.position ,
                    mail: response.mail ,
                    phoneNumber: response.phoneNumber ,
                    name: response.name ,
                    family: response.family ,
                    address: response.address
                }
                res.status(200).send(body);

            } else {
                res.status(404).send('member not found')
            }
        }).catch((e)=>{
            console.log(e)
            res.status(404).send(err);

        })
    } catch (e) {
        console.log(e , 'eee')
        res.status(500).send(e)
    }
};
exports.updateMember = function (req,res) {

    let username = req.params.username

    try {
        let BearerToken = req.headers.authorization;
        let token = BearerToken.replace('Bearer ','');
        let decodedToken = jwt.verify(token, process.env.SECRETKEY);
        let _id = decodedToken._id;
        var body = _.pick(req.body, ['name','family','mail','phoneNumber','position','address']);
        Member.findOneAndUpdate({username: username , _id: _id} , body , {new: true} ).then((response)=>{
            if (response) {
                let body = {
                    position: response.position ,
                    mail: response.mail ,
                    phoneNumber: response.phoneNumber ,
                    name: response.name ,
                    family: response.family ,
                    address: response.address
                }
                res.status(200).send(body);

            }
        }).catch((e)=>{
            res.status(404).send(err);

        })
    } catch (e) {
        res.status(500).send(e)
    }
};


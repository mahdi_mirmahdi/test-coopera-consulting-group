const _ = require('lodash');
const loadTest = require('loadtest');


exports.getTestResponse = async function (req,res) {
    try {
        let token = req.headers.authorization;

        let options = {
            url: 'http://193.26.14.135/user/get',
            maxRequests: 2000,
            headers: {
                "authorization": token
            },
            requestsPerSecond: 200
        };
        loadTest.loadTest(options, function(error, result) {
            if (error)
            {
                res.status(400).send(error)

            }
            res.status(200).send(result)
        });
    } catch (e) {
        res.status(500).send(e)
    }


};


const expect = require('expect');
const request = require('supertest');

const {app} = require('./../server');
const {User} = require('../models/User');
const {Course} = require('../models/Course');
const {Step} = require('../models/Step');
const {Member} = require('../models/Member');
const {Note} = require('../models/Note');
const {ObjectID} = require('mongodb')
let token ;
let username ;
let userCredentials = {
    username: "carna" ,
    password: "123123123"
}

beforeEach((done) => {
    request(app)
        .post('/api/member/login')
        .send(userCredentials)
        .expect(200)
        .end((err , res) => {
            if (err) {
                return done(err)
            }
            token = res.body.token
            username= res.body.username
            done()
        })
})
describe('Member Model' , () => {

    it('## /member/login  ==> should be Login', function (done) {
        let userCredentials = {
            username: "carna" ,
            password: "123123123"
        }
        request(app)
            .post('/api/member/login')
            .send(userCredentials)
            .expect(200)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()
            })


    });
    it('   * username or password is invalid', function (done) {
        let userCredentials = {
            username: "car2na" ,
            password: "123123123"
        }
        request(app)
            .post('/api/member/login')
            .send(userCredentials)
            .expect(401)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()

            })


    });

    it('## /member/find/:username  ==> should be find a member', function (done) {
        let memberUsername = 'carna'
        request(app)
            .get(`/api/member/find/${memberUsername}`)
            .set('authorization', token)
            .set('username', username)
            .expect(200)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()

            })


    });
    it('   *username is invalid', function (done) {
        let memberUsername = 'carnas'
        request(app)
            .get(`/api/member/find/${memberUsername}`)
            .set('authorization', token)
            .set('username', username)
            .expect(404)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()

            })


    });
    it('    **should not be able to consume the route /member/find/:username since no token was sent', function (done) {
        let memberUsername = 'carna'
        request(app)
            .get(`/api/member/find/${memberUsername}`)
            .expect(401)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()

            })


    });

})
describe("User Model" , () => {

    it('## /user/create  ==> should create a new User', function (done) {
        let body = {
            name : "test" ,
            family: "mirmahdi" ,
            mail : "mahdimirmahdi@yahoo.com" ,
            phoneNumber: "5446912796" ,
            position : "developer" ,
            salary : "10000" ,
            projectNumber : 2
        }
            request(app)
                .post('/api/user/create')
                .set('authorization', token)
                .set('username', username)
                .send(body)
                .expect(200)
                .expect((res) => {
                    expect(res.body.mail).toBe(body.mail)
                })
                .end((err , res) => {
                    if(err) {
                        return done(err);
                    }
                    User.findOne({name: body.name}).then((user) => {
                        expect(user.name).toBe(body.name);
                        expect(user.family).toBe(body.family);
                        expect(user.mail).toBe(body.mail);
                        expect(user.phoneNumber).toBe(body.phoneNumber);
                        expect(user.position).toBe(body.position);
                        expect(user.salary).toBe(body.salary);
                        expect(user.projectNumber).toBe(body.projectNumber);
                        done();
                    }).catch((e) => done(e));

                })


    });
    it('   *should not be able to consume the route /user/create since no token was sent', function (done) {
        let body = {
            name : "mahdi" ,
            family: "mirmahdi" ,
            mail : "mahdimirmahdi@yahoo.com" ,
            phoneNumber: "5446912796" ,
            position : "developer" ,
            salary : "10000" ,
            projectNumber : 2
        }
        request(app)
            .post('/api/user/create')
            .send(body)
            .expect(401)
            .end((err , res) => {
                if(err) {
                    return done(err);
                }
            })
        done()
    });
    it('    **should not be create User with invalid body /user/create', function (done) {
        request(app)
            .post('/api/user/create')
            .set('authorization', token)
            .set('username', username)
            .send({})
            .expect(400)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
             done()
            })
    });
    it('## /user/get  ==> should Get all User ', function (done) {
        request(app)
            .get('/api/user/get')
            .set('authorization', token)
            .set('username', username)
            .expect(200)
            .expect((res) => {
                User.find().then((users) => {
                    expect(res.body.length).toBe(users.length)
                })
            })
            .end(done)
    });
    it('   *should not be able to consume the route /user/get since no token was sent', function (done) {
        request(app)
            .get('/api/user/get')
            .expect(401)
            .end((err , res) => {
                if(err) {
                    return done(err);
                }
            })
        done()
    });
    it('## /user/update/:id  ==> should update the user', function (done) {
        User.findOne({name : 'test'}) .then((user) => {
            let Id = user._id
            request(app)
                .patch(`/api/user/update/${Id}`)
                .set('authorization', token)
                .set('username', username)
                .send({
                    name: 'munes' ,
                    family : 'abbasi'
                })
                .expect(200)
                .end((err , res) => {
                    if (err) {
                        return done(err)
                    }
                    User.findById(Id).then((user) => {
                        expect(res.body.name).toBe('munes')
                        expect(res.body.family).toBe('abbasi')
                        expect(user.name).toBe('munes')
                        expect(user.family).toBe('abbasi')
                        done()
                    }) .catch((e) => done(e))
                })

        })

    });
    it('   *should not be able to consume the route /user/update since no token was sent', function (done) {
            let Id = "asdmxaskdmasklds"
            request(app)
                .patch(`/api/user/update/${Id}`)
                .send({
                    name: 'munes' ,
                    family : 'abbasi'
                })
                .expect(401)
                .end((err , res) => {
                    if (err) {
                        return done(err)
                    }
                    done()

                })



    });
    it('    **should not be update User with invalid Id', function (done) {
            let Id = "askdkldnqw"
            request(app)
                .patch(`/api/user/update/${Id}`)
                .set('authorization', token)
                .set('username', username)
                .send({
                    name: 'munes' ,
                    family : 'abbasi'
                })
                .expect(404)
                .end((err , res) => {
                    if (err) {
                        return done(err)
                    }
                    done()
                })


    });
    it('     ***user not found', function (done) {
        let Id = '609d30ee4c0d1e16bce962fb'
        request(app)
            .patch(`/api/user/update/${Id}`)
            .set('authorization', token)
            .set('username', username)
            .expect(404)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()
            })


    });
    it('## /user/delete/:id  ==> should delete a user ', function (done) {
        User.findOne({name : 'munes'}) .then((user) => {
            let Id = user._id
            request(app)
                .delete(`/api/user/delete/${Id}`)
                .set('authorization', token)
                .set('username', username)
                .expect(200)
                .expect((res) => {
                    expect(res.body).toBe(true)
                })
                .end((err , res) => {
                    if (err) {
                        return done(err)
                    }
                    User.findById(Id).then((user) => {
                        expect(user).toBeNull()
                        done()
                    }) .catch((e) => done(e))
                })

        })

    });
    it('   *should not be delete a user with invalid Id', function (done) {
            let Id = 'akndqwnfqwnfd'
            request(app)
                .delete(`/api/user/delete/${Id}`)
                .set('authorization', token)
                .set('username', username)
                .expect(404)
                .end((err , res) => {
                    if (err) {
                        return done(err)
                    }
                   done()
                })


    });
    it('    **user not found', function (done) {
            let Id = '609d30ee4c0d1e16bce962fb'
            request(app)
                .delete(`/user/delete/${Id}`)
                .set('authorization', token)
                .set('username', username)
                .expect(404)
                .end((err , res) => {
                    if (err) {
                        return done(err)
                    }
                   done()
                })


    });
})
describe("Note Model" , () => {

    it('## /note/create  ==> should create a new Note', function (done) {
        let body = {
            username : "carna" ,
            color : "red" ,
            text : "test"
        }
        Member.findOne({username : body.username}).then((member) => {

            request(app)
                .post('/api/note/create')
                .set('authorization', token)
                .set('username', username)
                .send(body)
                .expect(200)
                .expect((res) => {
                    expect(res.body.text).toBe(body.text)
                    expect(res.body.color).toBe(body.color)
                    expect(JSON.stringify(res.body.username)).toBe(JSON.stringify(member._id))
                })
                .end((err , res) => {
                    if(err) {
                        return done(err);
                    }
                    Note.findOne({_id: res.body._id}).then((note) => {
                        expect(note.text).toBe(body.text);
                        expect(note.color).toBe(body.color);
                        expect(JSON.stringify(note.username)).toBe(JSON.stringify(member._id));
                        done();
                    }).catch((e) => done(e));

                })
        }).catch((e) => done(e))




    });
    it('   * should not be create Note with invalid user /note/create', function (done) {
        let body = {
            username : "invalid" ,
            color : "red" ,
            text : "carna note"
        }
        Member.findOne({username : body.username}).then((member) => {
            request(app)
                .post('/api/note/create')
                .set('authorization', token)
                .set('username', username)
                .send(body)
                .expect(401)
                .end((err , res) => {
                    if(err) {
                        return done(err);
                    }
                    done()
                })
        }).catch((e) => done(e))




    });
    it('    ** should not be able to consume the route /note/create since no token was sent', function (done) {
        let body = {
            username : "invalid" ,
            color : "red" ,
            text : "carna note"
        }
        Member.findOne({username : body.username}).then((member) => {
            request(app)
                .post('/api/note/create')
                .send(body)
                .expect(401)
                .end((err , res) => {
                    if(err) {
                        return done(err);
                    }
                    done()
                })
        }).catch((e) => done(e))




    });
    it('     *** should not be create Note with invalid body /note/create', function (done) {
        let body = {
            username : "carna" ,
            text : "carna note"
        }
        Member.findOne({username : body.username}).then((member) => {
            request(app)
                .post('/api/note/create')
                .set('authorization', token)
                .set('username', username)
                .send(body)
                .expect(400)
                .end((err , res) => {
                    if(err) {
                        return done(err);
                    }
                    done()
                })
        }).catch((e) => done(e))




    });
    it('## /note/get/:username  ==> should Get the note ', function (done) {
        let creatorUsername = 'carna';
              request(app)
                .get(`/api/note/get/${creatorUsername}`)
                .set('authorization', token)
                .set('username', username)
                .expect(200)
                .expect((res) => {
                    Member.findOne({username: creatorUsername}).then((member) => {
                        for(let note of res.body) {
                            expect(JSON.stringify(note.username)).toBe(JSON.stringify(member._id))
                        }
                    }).catch((e) => done(e))
                })
                .end(done)
    });
    it('   * should not be able to consume the route /note/get/:username since no token was sent', function (done) {
        let creatorUsername = 'carna';
              request(app)
                .get(`/api/note/get/${creatorUsername}`)
                .expect(401)
                .end(done)
    });
    it('    ** should not be get Note with invalid Username /note/create', function (done) {
        let creatorUsername = 'carnas';
        request(app)
            .get(`/api/note/get/${creatorUsername}`)
            .set('authorization', token)
            .set('username', username)
            .expect(401)
            .end((err ,res) => {
                if (err) {
                    return done(err)
                }
                done()
            })
    });
    it('## /note/update/:username/:id  ==> should update a Note', function (done) {
        let body = {
            color : "red" ,
            text : "test mocha",
            type : "text"
        }
        let username = 'carna'
        Note.findOne({text : 'test'}).then((note) => {
            if (note) {
                let id = note._id
                request(app)
                    .patch(`/api/note/update/${username}/${id}`)
                    .set('authorization', token)
                    .set('username', username)
                    .send(body)
                    .expect(200)
                    .expect((res) => {
                        expect(res.body).toBe(true)
                    })
                    .end((err , res) => {
                        if(err) {
                            return done(err);
                        }
                        Note.findOne({_id: id}).then((note) => {
                            expect(note.text).toBe(body.text);
                            expect(note.color).toBe(body.color);
                            done();
                        }).catch((e) => done(e));

                    })
            }else{
                return done('note not found')
            }
        }).catch((e) => done(e))




    });
    it('   * should not be able to consume the route /note/update/:username/:id since no token was sent', function (done) {
        let body = {
            color : "red" ,
            text : "test mocha",
            type : "text"
        }
        let username = 'carna'
        Note.findOne({text : 'test mocha'}).then((note) => {
            if (note) {
                let id = note._id
                request(app)
                    .patch(`/api/note/update/${username}/${id}`)
                    .send(body)
                    .expect(401)
                    .end((err , res) => {
                        if(err) {
                            return done(err);
                        }
                        done()
                    })
            }else{
                return done('note not found')
            }
        }).catch((e) => done(e))
    });
    it('    ** username not found', function (done) {
        let creatorUsername= 'carnas';
        let Id = '609c175ca9d18e0b9829642b'
        request(app)
            .patch(`/api/note/update/${creatorUsername}/${Id}`)
            .set('authorization', token)
            .set('username', username)
            .expect(401)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()
            })

    });
    it('     *** note body type is invalid', function (done) {
        let creatorUsername= 'carna';
        let Id = '609c175ca9d18e0b9829642b'
        request(app)
            .patch(`/api/note/update/${creatorUsername}/${Id}`)
            .set('authorization', token)
            .set('username', username)
            .expect(400)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()
            })

    });
    it('      **** note id is invalid', function (done) {
        let creatorUsername= 'carna';
        let body = {
            type: 'color'
        }
        let Id = '609c175ca9d18e0b9829642b'
        request(app)
            .patch(`/api/note/update/${creatorUsername}/${Id}`)
            .set('authorization', token)
            .set('username', username)
            .send(body)
            .expect(404)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()
            })

    });
    it('       ***** note id is not type of ObjectId', function (done) {
        let creatorUsername= 'carna';
        let Id = '609c19d18e0b9829642b'
        request(app)
            .patch(`/api/note/update/${creatorUsername}/${Id}`)
            .set('authorization', token)
            .set('username', username)
            .expect(404)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()
            })

    });





    it('## /note/delete/:username/:id  ==> should delete a Note ', function (done) {
        let creatorUsername= 'carna';
        Note.findOne({text : 'test mocha'}) .then((note) => {
            if (note) {
                let Id = note._id
                request(app)
                    .delete(`/api/note/delete/${creatorUsername}/${Id}`)
                    .set('authorization', token)
                    .set('username', username)
                    .expect(200)
                    .expect((res) => {
                        expect(res.body).toBe(true)
                    })
                    .end((err , res) => {
                        if (err) {
                            return done(err)
                        }
                        Note.findById(Id).then((note) => {
                            expect(note).toBeNull()
                            done()
                        }) .catch((e) => done(e))
                    })
            } else {
                return done('note not found')
            }
        })

    });
    it('   * should not be able to consume the route /note/delete/:username/:id since no token was sent', function (done) {
        let creatorUsername= 'carna';
        request(app)
            .delete(`/api/note/delete/${creatorUsername}/asmaocms`)
            .expect(401)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()
            })


    });
    it('    ** username not found', function (done) {
        let creatorUsername= 'carnas';
        let Id = '609c175ca9d18e0b9829642b'
        request(app)
            .delete(`/api/note/delete/${creatorUsername}/${Id}`)
            .set('authorization', token)
            .set('username', username)
            .expect(401)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()
            })

    });
    it('     *** note id is invalid', function (done) {
        let creatorUsername= 'carna';
        let Id = '609c175ca9d18e0b9829642b'
        request(app)
            .delete(`/api/note/delete/${creatorUsername}/${Id}`)
            .set('authorization', token)
            .set('username', username)
            .expect(400)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()
            })

    });
    it('      **** note id is not type of objectId', function (done) {
        let creatorUsername= 'carna';
        let Id = '609c175ca9d18e0b982b'
        request(app)
            .delete(`/api/note/delete/${creatorUsername}/${Id}`)
            .set('authorization', token)
            .set('username', username)
            .expect(404)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()
            })

    });

})
describe("Course Model" , () => {
    it( '## /course/create  ==> Should be Create a New Course', function (done) {
        let body = {
            name: "test"
        }
        request(app)
            .post('/api/course/create')
            .set('authorization', token)
            .set('username', username)
            .send(body)
            .expect(200)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()
            })

    });
    it( '   *should not be able to consume the route /course/create since no token was sent', function (done) {
        let body = {
            name: "test"
        }
        request(app)
            .post('/api/course/create')
            .send(body)
            .expect(401)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()
            })

    });
    it( '    **should not be create Course with invalid body', function (done) {
        let body = {
            color: "test"
        }
        request(app)
            .post('/api/course/create')
            .set('authorization', token)
            .set('username', username)
            .send(body)
            .expect(409)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()
            })

    });
    it( '## /course/get  ==> Should be find a Course', function (done) {

        request(app)
            .get('/api/course/get')
            .set('authorization', token)
            .set('username', username)
            .expect(200)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()
            })

    });
    it( '   *should not be able to consume the route /course/get since no token was sent', function (done) {

        request(app)
            .get('/api/course/get')
            .expect(401)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()
            })

    });
    it('## /course/update/:id  ==> should update the course', function (done) {
        Course.findOne({name : 'test'}) .then((course) => {
            let Id = course._id
            request(app)
                .patch(`/api/course/update/${Id}`)
                .set('authorization', token)
                .set('username', username)
                .send({
                    name: 'test mocha'
                })
                .expect(200)
                .end((err , res) => {
                    if (err) {
                        return done(err)
                    }
                    Course.findById(Id).then((course) => {
                        expect(course.name).toBe('test mocha')
                        done()
                    }) .catch((e) => done(e))
                })

        })

    });
    it('   *should not be able to consume the route /course/update/:id since no token was sent', function (done) {
        let Id = "asdmxaskdmasklds"
        request(app)
            .patch(`/api/course/update/${Id}`)
            .send({
                name: 'munes' ,
            })
            .expect(401)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()

            })



    });
    it('    **should not be update Course with invalid Id', function (done) {
        let Id = "askdkldnqw"
        request(app)
            .patch(`/api/course/update/${Id}`)
            .set('authorization', token)
            .set('username', username)
            .send({
                name: 'munes'
            })
            .expect(404)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()
            })


    });
    it('     ***course not found', function (done) {
        let Id = '609d30ee4c0d1e16bce962fb'
        request(app)
            .patch(`/api/course/update/${Id}`)
            .set('authorization', token)
            .set('username', username)
            .expect(404)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()
            })


    });
    it('## /course/delete/:id  ==> should delete a course ', function (done) {
        Course.findOne({name : 'test mocha'}) .then((course) => {
            let Id = course._id
            request(app)
                .delete(`/api/course/delete/${Id}`)
                .set('authorization', token)
                .set('username', username)
                .expect(200)
                .end((err , res) => {
                    if (err) {
                        return done(err)
                    }
                    Course.findById(Id).then((course) => {
                        expect(course).toBeNull()
                        done()
                    }) .catch((e) => done(e))
                })

        })

    });
    it('   *should not be delete a Course with invalid Id', function (done) {
        let Id = 'akndqwnfqwnfd'
        request(app)
            .delete(`/api/course/delete/${Id}`)
            .set('authorization', token)
            .set('username', username)
            .expect(404)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()
            })


    });
    it('    **course not found', function (done) {
        let Id = '609d30ee4c0d1e16bce962fb'
        request(app)
            .delete(`/api/user/delete/${Id}`)
            .set('authorization', token)
            .set('username', username)
            .expect(404)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()
            })


    });

})
describe("Step Model" , () => {
    it( '## /course/step/create  ==> Should be Create a New Step', function (done) {

        Course.create({name: 'test step'}).then((response) => {
            let body = {
                course: response._id
            }
            request(app)
                .post('/api/course/step/create')
                .set('authorization', token)
                .set('username', username)
                .send(body)
                .expect(200)
                .end((err , res) => {
                    if (err) {
                        return done(err)
                    }
                    Course.findOneAndDelete({_id: response._id}).then((response) => {
                        if (response) {
                            Step.findOneAndDelete({_id: res.body._id}).then((responseStep) => {
                                if (responseStep) {
                                    done()
                                }
                                else {
                                    return done(err)

                                }
                            }).catch((e) => done(e))

                        }
                        else {
                            return done(err)

                        }

                    }).catch((e) => done(e))
                })
        })


    });
    it( '   *should not be able to consume the route /course/step/create since no token was sent', function (done) {
        let body = {
            course: '609e6355a1d4950494a42781'
        }
        request(app)
            .post('/api/course/step/create')
            .send(body)
            .expect(401)
            .end((err , res) => {
                if (err) {
                    return done(err)
                }
                done()

            })

    });
    it( '    **course not found', function (done) {
            let body = {
                course: '609e6355a1d4950494a42781'
            }
            request(app)
                .post('/api/course/step/create')
                .set('authorization', token)
                .set('username', username)
                .send(body)
                .expect(404)
                .end((err , res) => {
                    if (err) {
                        return done(err)
                    }
                    done()

                })




    });
})




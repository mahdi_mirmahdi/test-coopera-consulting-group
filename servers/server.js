const dotenv = require('dotenv');
const express = require('express');
var bodyParser = require('body-parser');
dotenv.config();
var cors = require('cors');
const app = express();
var memberRouts = require('./routers/member');
var noteRouts = require('./routers/note');
var userRouts = require('./routers/user');
var courseRouts = require('./routers/course');
var testRouts = require('./routers/test');
let {mongoose} = require('./db/mongoose')
app.use(bodyParser.json(),cors());

app.use('/api/member', memberRouts );
app.use('/api/note', noteRouts );
app.use('/api/user', userRouts );
app.use('/api/course', courseRouts );
app.use('/api/loadtest', testRouts );

///listen port 3000
app.listen(process.env.PORT,()=>{
    console.log(`App listen on port ${process.env.PORT} `)
});



module.exports = {app}






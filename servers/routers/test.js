
let express = require('express');
let router = express.Router();
let testLoad = require('../controller/testLoad');
let autMiddleware = require('../middle/adminauth');

//load test router
router.get('/get', autMiddleware.authentication  , testLoad.getTestResponse, (req,res)=>{});


module.exports = router;


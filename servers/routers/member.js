
var express = require('express');
var router = express.Router();
var memberController = require('../controller/member');

var autMiddleware = require('../middle/adminauth');



//member router
router.post('/login' ,memberController.login, (req,res)=>{});
router.get('/find/:username' , autMiddleware.authentication ,memberController.getMember, (req,res)=>{});
router.patch('/update/:username' , autMiddleware.authentication ,memberController.updateMember, (req,res)=>{});



module.exports = router;



let express = require('express');
let router = express.Router();
let userController = require('../controller/user');
let autMiddleware = require('../middle/adminauth');


//user router
router.get('/get' , autMiddleware.authentication ,  userController.getUser, (req,res)=>{});
router.patch('/update/:id' , autMiddleware.authentication , userController.update, (req,res)=>{});
router.delete('/delete/:id' , autMiddleware.authentication , userController.delete, (req,res)=>{});
router.post('/create', autMiddleware.authentication  , userController.create, (req,res)=>{});
router.post('/import', autMiddleware.authentication  , userController.importData, (req,res)=>{});



module.exports = router;


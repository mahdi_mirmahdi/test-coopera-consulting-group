
var express = require('express');
var router = express.Router();
var noteController = require('../controller/note');

var autMiddleware = require('../middle/adminauth');


//note router
router.post('/create' , autMiddleware.authentication ,noteController.create, (req,res)=>{});
router.get('/get/:username' , autMiddleware.authentication ,noteController.getNote, (req,res)=>{});
router.patch('/update/:username/:id', autMiddleware.authentication ,noteController.update, (req,res)=>{});
router.delete('/delete/:username/:id', autMiddleware.authentication ,noteController.delete, (req,res)=>{});



module.exports = router;



let express = require('express');
let router = express.Router();
let courseController = require('../controller/course');
let autMiddleware = require('../middle/adminauth');

//course router
router.get('/get', autMiddleware.authentication  , courseController.getCourse, (req,res)=>{});
router.post('/create' , autMiddleware.authentication , courseController.createCourse, (req,res)=>{});
router.patch('/update/:id' , autMiddleware.authentication , courseController.courseUpdateById, (req,res)=>{});
router.delete('/delete/:id' , autMiddleware.authentication , courseController.deleteCourseById, (req,res)=>{});


//step router
router.post('/step/create' , autMiddleware.authentication  , courseController.createCourseStep, (req,res)=>{});
router.delete('/step/delete/:id' , autMiddleware.authentication , courseController.stepDeleteById, (req,res)=>{});


//question router
router.post('/question/create' , autMiddleware.authentication  , courseController.createStepQuestion, (req,res)=>{});
router.delete('/question/delete/:id' , autMiddleware.authentication , courseController.questionDeleteById, (req,res)=>{});
router.patch('/question/update/:id' , autMiddleware.authentication , courseController.questionUpdateById, (req,res)=>{});


module.exports = router;

